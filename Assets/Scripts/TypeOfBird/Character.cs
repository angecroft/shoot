﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Character script contains the primitives atributes of any kind of bird.
/// </summary>
public class Character : MonoBehaviour 
{
	public enum Relationship{
		FRIEND,
		ENEMY,
		PLAYER
	};

	public GameObject target;

	private Relationship relationshipWithThePlayer;
	private int health = 10;
	private Enemy enemyScript;	
	private Vector3 _movement, _direction;
	private float speed = 0.05f;


	/// <summary>
	/// Inits the bird with a position and a relationship status.
	/// </summary>
	/// <param name="relationship">Relationship.</param>
	/// <param name="position">Position.</param>
	public void InitBird(Relationship relationship, Vector3 position)
	{
		relationshipWithThePlayer = relationship;
		transform.position = position;
		_direction = (-1) * position;
	}

	/// <summary>
	/// Gets the relationship with the player.
	/// </summary>
	/// <returns>The relationship.</returns>
	public Relationship GetRelationship() { return relationshipWithThePlayer; }

	void Start()
	{
		enemyScript = GameObject.Find("Scripts").GetComponent<Enemy>();
	}

	void OnTriggerEnter(Collider collider)
	{
		Projectile projectile = collider.gameObject.GetComponent<Projectile>();
		if (projectile != null)
		{
			if(relationshipWithThePlayer.Equals(Character.Relationship.ENEMY))
			{
				LossOfHealth(projectile);
				Destroy(projectile.gameObject);
			}	
			if(relationshipWithThePlayer.Equals(Character.Relationship.FRIEND))
			{
				if(!projectile.GetParent().Equals(Character.Relationship.FRIEND))
				{
					LossOfHealth(projectile);
					Destroy(projectile.gameObject);
				}					
			}	
		}
	}

	void Update(){ Move(); }	

	void FixedUpdate()
	{ 
		if(Relationship.ENEMY.Equals(relationshipWithThePlayer))
			rigidbody.velocity = _movement; 
	}
	
	/// <summary>
	/// Moves the bird with the given direction.
	/// </summary>
	private void Move()
	{ 
		if(Relationship.ENEMY.Equals(relationshipWithThePlayer))
			_movement = new Vector3(speed * _direction.x, speed * _direction.y, 0); 
	}

	/// <summary>
	/// Loss of health of the bird and destruction of it when it has no health anymore.
	/// In the future this is where an animation can be launch before the bird disapear.
	/// </summary>
	/// <param name="projectile">Projectile.</param>
	private void LossOfHealth(Projectile projectile)
	{
		health -= projectile.damage;
		if(health <= 0)
		{
			if(relationshipWithThePlayer.Equals(Character.Relationship.ENEMY))
			{
				enemyScript.listOfEnnemies.Remove(gameObject);
				Score.Instance.AddScore();
				Destroy(gameObject);
			}
		}			
	}
}
