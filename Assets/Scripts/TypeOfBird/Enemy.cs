﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Everything about the enemies of the player.
/// </summary>
public class Enemy : MonoBehaviour 
{
	public GameObject birdPrefab;
	public List<GameObject> listOfEnnemies;
}
