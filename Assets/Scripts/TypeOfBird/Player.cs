﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This script handle the player's actions.
/// </summary>
public class Player : MonoBehaviour 
{
	public GameObject birdPrefab;
	private Rotation rotateTheBird;
	private Shoot shooter;
	private GazeCamera _gazeCam;

	void Start () 
	{
		_gazeCam = Camera.main.GetComponent<GazeCamera>();
		GameObject birdPlayer = Instantiate(birdPrefab) as GameObject;
		birdPlayer.GetComponent<Character>().InitBird(Character.Relationship.PLAYER, new Vector3(0,0,0));
		rotateTheBird = birdPlayer.GetComponent<Rotation>();
		shooter = birdPlayer.GetComponent<Shoot>();
	}
	
	void Update () 
	{
		rotateTheBird.TurnTowards(_gazeCam.gazeIndicator.transform.position);
		AttackTowards(_gazeCam.gazeIndicator.transform.position);
	}

	/// <summary>
	/// Attacks towards the gaze indicator. The direction of the attack must be normalized in order to have a movement 
	/// of the projectile that will be constant by velocity in any direction.
	/// </summary>
	/// <param name="directionNotNormalized">Direction not normalized yet.</param>
	private void AttackTowards(Vector3 directionNotNormalized)
	{
		Vector2 directionNormalized = new Vector2(directionNotNormalized.x, directionNotNormalized.y);
		directionNormalized = directionNormalized.normalized;
		shooter.Attack(directionNormalized, Character.Relationship.PLAYER);
	}
}
