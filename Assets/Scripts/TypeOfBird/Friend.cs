﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
///  Everything about the friends of the player.
/// </summary>
public class Friend : MonoBehaviour 
{
	public GameObject birdPrefab;
	private Shoot shooter;
	private GameObject birdFriend;
	private Enemy enemyScript;
	private Rotation rotateTheBird;
	private List<GameObject> listOfEnnemies;
	private Character characterScript;

	void Start () 
	{
		birdFriend = Instantiate(birdPrefab) as GameObject;
		characterScript = birdFriend.GetComponent<Character>();
		characterScript.InitBird(Character.Relationship.FRIEND, new Vector3(-2,0,0));
		shooter = birdFriend.GetComponent<Shoot>();
		enemyScript = GameObject.Find("Scripts").GetComponent<Enemy>();
		listOfEnnemies = enemyScript.listOfEnnemies;
		rotateTheBird = birdFriend.GetComponent<Rotation>();
	}

	void Update()
	{
		AttackAnEnemy();
	}

	/// <summary>
	/// Attacks towards a direction. The direction of the attack must be normalized in order to have a movement 
	/// of the projectile that will be constant by velocity in any direction.
	/// </summary>
	/// <param name="directionNotNormalized">Direction not normalized yet.</param>
	private void AttackTowards(Vector3 directionNotNormalized)
	{
		Vector2 directionNormalized = new Vector2(directionNotNormalized.x, directionNotNormalized.y);
		directionNormalized = directionNormalized.normalized;
		shooter.Attack(directionNormalized, Character.Relationship.FRIEND);
	}

	/// <summary>
	/// Attacks an enemy from the list of ennemies. An enemy is a target of a bird friend while it is not dead.
	/// </summary>
	private void AttackAnEnemy()
	{
		if(characterScript.target == null && listOfEnnemies.Count != 0)
		{
			int selectAnEnemy = Random.Range(0, listOfEnnemies.Count);
			Vector3 directionOfEnemy = listOfEnnemies[selectAnEnemy].transform.position;
			directionOfEnemy -= birdFriend.transform.position;
			AttackTowards(directionOfEnemy);
			rotateTheBird.TurnTowards(directionOfEnemy);
			characterScript.target = listOfEnnemies[selectAnEnemy];
		} else if(characterScript.target != null)
		{
			Vector3 directionOfEnemy = characterScript.target.transform.position;
			directionOfEnemy -= birdFriend.transform.position;
			AttackTowards(directionOfEnemy);
			rotateTheBird.TurnTowards(directionOfEnemy);
		}
	}
}
