﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script of a projectile. A projectile can move, has a velocity, a direction, an ability to do damage.
/// </summary>
public class Projectile : MonoBehaviour 
{
	public int damage {get; private set;}
	private float speed = 2;	
	private Vector3 _movement, _direction;
	private Character.Relationship _parent;

	/// <summary>
	/// Inits the projectile.
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void InitProjectile(Vector3 direction, Character.Relationship parent) 
	{ 
		_direction = direction; 
		_parent = parent;
	}

	public Character.Relationship GetParent(){ return _parent; }

	void Start()
	{ 
		damage = 2;
		Destroy(gameObject, 5); 
	}
	void Update(){ MoveProjectile(); }	
	void FixedUpdate(){ rigidbody.velocity = _movement; }

	/// <summary>
	/// Moves the projectile with the given direction.
	/// </summary>
	private void MoveProjectile(){ _movement = new Vector3(speed * _direction.x, speed * _direction.y, 0); }
}
