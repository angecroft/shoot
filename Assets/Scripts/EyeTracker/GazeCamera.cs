﻿/*
 * Copyright (c) 2013-present, The Eye Tribe. 
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the LICENSE file in the root directory of this source tree. 
 *
 */

using UnityEngine;
using System.Collections;
using TETCSharpClient;
using TETCSharpClient.Data;
using Assets.Scripts;

/// <summary>
/// Component attached to 'Main Camera' of '/Scenes/std_scene.unity'.
/// This script handles the navigation of the 'Main Camera' according to 
/// the GazeData stream recieved by the EyeTribe Server.
/// </summary>
public class GazeCamera : MonoBehaviour, IGazeListener
{
	#region Variables

    private Camera cam;

    private double eyesDistance;
    private double depthMod;

    private Collider currentHit;

    private GazeDataValidator gazeUtils;

	public GameObject gazeIndicator; //{get; set;}

	#endregion

    void Start()
    {
        //Stay in landscape
        Screen.autorotateToPortrait = false;

        cam = GetComponent<Camera>();

        //initialising GazeData stabilizer
        gazeUtils = new GazeDataValidator(30);
        
		//activate C# TET client, default port
		if(false == GazeManager.Instance.IsActivated)
		{
			GazeManager.Instance.Activate
			(
				GazeManager.ApiVersion.VERSION_1_0,
				GazeManager.ClientMode.Push
			);
		}
		//register this class for events and gaze updates
        GazeManager.Instance.AddGazeListener(this);

		gazeIndicator = GameObject.Instantiate(Resources.Load("croix")) as GameObject;
		gazeIndicator.transform.position = Vector3.zero;
    }

    public void OnGazeUpdate(GazeData gazeData)
    {
        //Add frame to GazeData cache handler
        gazeUtils.Update(gazeData);
    }
	
    void Update()
    {
        Point2D gazeCoords = gazeUtils.GetLastValidSmoothedGazeCoordinates();

        if (null != gazeCoords)
        {
            //map gaze indicator
            Point2D gp = UnityGazeUtils.getGazeCoordsToUnityWindowCoords(gazeCoords);

            Vector3 screenPoint = new Vector3((float)gp.X, (float)gp.Y, cam.nearClipPlane + .1f);
			Vector3 planeCoord = cam.ScreenToWorldPoint(new Vector3(screenPoint.x, screenPoint.y, 1));

			if(planeCoord.x < -1) planeCoord.x = -1;
			else if(planeCoord.x > 1) planeCoord.x = 1;
			if(planeCoord.y < -0.5f) planeCoord.y = -0.5f;
			else if(planeCoord.y > 0.5f) planeCoord.y = 0.5f;

			gazeIndicator.transform.position = planeCoord;

            //handle collision detection
            checkGazeCollision(screenPoint);
        }

        //handle keypress
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        else if (Input.GetKey(KeyCode.Space))
        {
            Application.LoadLevel(0);
        }
    }

	/// <summary>
	/// Checks the gaze collision.
	/// </summary>
	/// <param name="screenPoint">Screen point.</param>
    private void checkGazeCollision(Vector3 screenPoint)
    {
        Ray collisionRay = cam.ScreenPointToRay(screenPoint);
        RaycastHit hit;
        if (Physics.Raycast(collisionRay, out hit))
        {
            if (null != hit.collider && currentHit != hit.collider)
            {
                //switch colors of cubes according to collision state
                if (null != currentHit)
                    currentHit.renderer.material.color = Color.white;
                currentHit = hit.collider;
//				Destroy (currentHit.gameObject);
//                currentHit.renderer.material.color = Color.red;
            }
        }
    }

	#region GUI

    void OnGUI()
    {
        int padding = 10;
        int btnWidth = 160;
        int btnHeight = 40;
        int y = padding;

        if (GUI.Button(new Rect(padding, y, btnWidth, btnHeight), "Press to Exit"))
        {
            Application.Quit();
        }

        y += padding + btnHeight;

        if (GUI.Button(new Rect(padding, y, btnWidth, btnHeight), "Press to Re-calibrate"))
        {
            Application.LoadLevel(0);
        }
    }

	#endregion

    void OnApplicationQuit()
    {
        GazeManager.Instance.RemoveGazeListener(this);
    }
}
