﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component attached to every bird that should shoot an other one.
/// </summary>
public class Shoot : MonoBehaviour 
{
	public Transform shotPrefab;
	private float shootingRate = 0.5f;
	private float shootCooldown;

	/// <summary>
	/// Attacks towards a direction. This create a new projectile from the position of a bird to the given direction.
	/// </summary>
	/// <param name="directionNormalized">Direction normalized.</param>
	public void Attack(Vector2 directionNormalized, Character.Relationship parent)
	{
		if (CanAttack() && !directionNormalized.Equals(Vector2.zero))
		{
			shootCooldown = shootingRate;
			Transform shotTransform = Instantiate(shotPrefab) as Transform;
			shotTransform.position = transform.position;
			shotTransform.GetComponent<Rotation>().TurnTowards(directionNormalized);
			shotTransform.GetComponent<Projectile>().InitProjectile(directionNormalized, parent);
		}
	}

	void Start () { shootCooldown = 0f; }

	void FixedUpdate()
	{
		if (shootCooldown > 0) 
			shootCooldown -= Time.deltaTime;
	}

	/// <summary>
	/// Determines whether the bird can attack. Its can attack if the cooldown equals zero.
	/// </summary>
	/// <returns><c>true</c> if this instance can attack; otherwise, <c>false</c>.</returns>
	private bool CanAttack()
	{
		if(shootCooldown < 0.001f)
			return true;
		else
			return false;
	}
}
