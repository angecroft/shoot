﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manages the rotations of the game like the bird or the projectiles.
/// </summary>
public class Rotation : MonoBehaviour 
{
	private GazeCamera _gazeCam;
	
	void Start () 
	{
		_gazeCam = Camera.main.GetComponent<GazeCamera>();
	}

	/// <summary>
	/// Turns the gameObject towards an enemy.
	/// </summary>
	public void TurnTowards(Vector3 direction)
	{
		// calculation of the bird's rotation
		Vector2 vectorBase = Vector2.right; // (1, 0)
		if(_gazeCam == null) _gazeCam = Camera.main.GetComponent<GazeCamera>();

		// si le gaze indicator a des bonnes valeurs = debut de partie
		if(!direction.Equals(Vector3.zero))
		{
			// scalar product between vectorbase . direction
			float scalarProduct = (vectorBase.x * direction.x) + (vectorBase.y * direction.y);
			// lengths of the vector multiplied 
			float lengthMult = new Vector2(direction.x, direction.y).magnitude * vectorBase.magnitude;
			// result of the cosine angle in radians 
			float cosineAngle = scalarProduct / lengthMult;
			// the angle in radians
			float angleInRad = Mathf.Acos(cosineAngle);
			// the angle in degres
			float angle = (angleInRad * 180.0f) / (Mathf.PI); // convert rad to deg
			if(direction.y < 0) angle = angle * (-1);
			
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}
	}

	/// <summary>
	/// Turns the gameObject towards the nest.
	/// </summary>
	public void TurnTowards2()
	{
		Vector3 direction = transform.position;
		// calculation of the bird's rotation
		Vector2 vectorBase = Vector2.right; // (1, 0)
		
		// si le gaze indicator a des bonnes valeurs = debut de partie
		if(!direction.Equals(Vector3.zero))
		{
			// scalar product between vectorbase . direction
			float scalarProduct = (vectorBase.x * direction.x) + (vectorBase.y * direction.y);
			// lengths of the vector multiplied 
			float lengthMult = new Vector2(direction.x, direction.y).magnitude * vectorBase.magnitude;
			// result of the cosine angle in radians 
			float cosineAngle = scalarProduct / lengthMult;
			// the angle in radians
			float angleInRad = Mathf.Acos(cosineAngle);
			// the angle in degres
			float angle = (angleInRad * 180.0f) / (Mathf.PI); // convert rad to deg
			angle -= 180;
			if(direction.y < 0) angle = angle * (-1);
			
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}
	}
}
