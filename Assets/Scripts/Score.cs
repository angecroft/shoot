﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Scoring of the game
/// </summary>
public class Score : MonoBehaviour 
{
	public static Score Instance;
	private int score = 0;

	public void AddScore() { score += 50; }
	public int GetScore(){ return score; }
	public void Reset(){ score = 0; }

	void Start() { Instance = this; }
}
