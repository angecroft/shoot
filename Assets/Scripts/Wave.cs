﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A wave of ennemies is happening when a number of ennemies are spawn barely at the same time. A wave is over when 
/// the player kills all the enemies of it.
/// </summary>
public class Wave : MonoBehaviour 
{
	#region Variables
	/// <summary>
	/// Current number of unit instanced. 
	/// </summary>
	private int numberOfUnits;
	/// <summary>
	/// Number total of units that will be instanced per wave.
	/// </summary>
	private int totalOfUnits;
	/// <summary>
	/// The interval of time it takes between each wave.
	/// </summary>
	private float waveInterval;
	/// <summary>
	/// The time elapsed between two waves.
	/// </summary>
	private float timeBetweenWaveInterval;
	/// <summary>
	/// The interval of time it takes between each spawn of an enemy.
	/// </summary>
	private float spawnInterval;
	/// <summary>
	/// The time elapsed between two spawns.
	/// </summary>
	private float timeBetweenSpawns;
	/// <summary>
	/// The number total of waves in a game play.
	/// </summary>
	private int totalWaves;
	/// <summary>
	/// The current number of wave.
	/// </summary>
	private int numWave;
	/// <summary>
	/// Determines whether the spawn should spawn.
	/// </summary>
	private bool spawn;
	/// <summary>
	///  Used to determine if there is actual spawning to occur.
	/// </summary>
	private bool waveSpawn;
	private Enemy enemyScript;
	private Rotation rotateTheBird;

	#endregion

	void Start () 
	{
		numberOfUnits = 0;
		totalOfUnits = 5;
		waveInterval = 6.0f;
		timeBetweenWaveInterval = 0;
		spawnInterval = 1.0f;
		timeBetweenSpawns = 0;
		totalWaves = 10;
		numWave = 1;
		spawn = false;
		waveSpawn = true;
		enemyScript = GetComponent<Enemy>();
	}

	void FixedUpdate()
	{
		LaunchWave();
		Spawn();
	}

	/// <summary>
	/// Returns the number of waves left.
	/// </summary>
	public int WavesLeft
	{
		get
		{
			return totalWaves - numWave;
		}
	}

	/// <summary>
	/// Spawns a unit based on the level set.
	/// </summary>
	private GameObject SpawnUnit(GameObject bird)
	{
		GameObject birdSpawn = Instantiate(bird) as GameObject;
		birdSpawn.GetComponent<Character>().InitBird(Character.Relationship.ENEMY, randomPosition());
		rotateTheBird = birdSpawn.GetComponent<Rotation>();
		rotateTheBird.TurnTowards2();
		return birdSpawn;
	}

	/// <summary>
	/// Creates a random position to place enemies. They must not be placed to close to the nest.
	/// </summary>
	/// <returns>The position.</returns>
	private Vector3 randomPosition()
	{
		float randomX = Random.Range(2.00f, 5.00f);
		float randomY = Random.Range(0.80f, 3.00f);
		int sign = Random.Range(0,2);
		if(sign == 0)
			randomX =- randomX;
		sign = Random.Range(0,2);
		if(sign == 0)
			randomY =- randomY;
		return new Vector3(randomX, randomY, 0);
	}

	/// <summary>
	/// Spawn an instance of enemy if there is any that can be spawn.
	/// </summary>
	private void Spawn()
	{
		// An enemy can be spawn only if a wave can be spawn, and an instance, and there is still a wave to be launch.
		if(waveSpawn && spawn && WavesLeft > 0)
		{
			// If the total of units to spawn isn't already reached.
			if(numberOfUnits < totalOfUnits)
			{
				spawn = false;
				enemyScript.listOfEnnemies.Add(SpawnUnit(enemyScript.birdPrefab));
				++numberOfUnits;
			} else 
			{
				waveSpawn = false;
			}
		}
	}

	/// <summary>
	/// Launchs a wave if it can be launch at the current moment.
	/// </summary>
	private void LaunchWave()
	{
		// Triggers the counter between two waves.
		if(enemyScript.listOfEnnemies.Count == 0 && timeBetweenWaveInterval < waveInterval 
		   && !waveSpawn && WavesLeft > 0)
		{
			timeBetweenWaveInterval += Time.deltaTime;
		}else if(enemyScript.listOfEnnemies.Count == 0 && timeBetweenWaveInterval >= waveInterval && !waveSpawn)
		{
			ResetValuesToLaunchWave();
		}
		// A wave can be spawn.
		if(waveSpawn)
		{
			// Triggers the counter between two spawns of ennemies.
			if(!spawn && timeBetweenSpawns < spawnInterval)
			{
				timeBetweenSpawns += Time.deltaTime;
			} else
			{
				timeBetweenSpawns = 0;
				spawn = true;
			}
		}
	}

	/// <summary>
	/// Resets the values that need to be to launch a wave.
	/// </summary>
	private void ResetValuesToLaunchWave()
	{
		waveSpawn = true;
		++numWave;
		timeBetweenWaveInterval = 0;
		timeBetweenSpawns = 0;
		spawn = false;
		numberOfUnits = 0;
	}
}
